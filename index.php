<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
  <div class="container">
    <h2>Counting</h2>
    <p>Characters</p>
    <?php 
      include('inc/countCharacters.php');
      foreach($files as $file):
    ?>
    <table>
      <tr>
        <th>Filename</th>
        <th><?= $file['filename'];?></th>
      </tr>
      <tr>
        <td>Total Characters</td>
        <td><?= $file['characters']; ?></td>
      </tr>
    </table>
    <?php endforeach; ?>
  </div>
</body>
</html>
