<?php
header('Content-type: text/html; charset=utf-8');


//Add <pre></pre> tags to input file
function file_append ($filepath) {
    $fileContent = file_get_contents ($filepath);
    if(strpos($fileContent, "<pre>") === false){
        file_put_contents ($filepath, "<pre>\n" . $fileContent);
    } else if(strpos($fileContent, "</pre>") === false){
        file_put_contents($filepath, "\n</pre>" , FILE_APPEND | LOCK_EX);
    }   
}

$filepath = "files";  //Input File Path

foreach (glob("$filepath/*.txt") as $filename) :

file_append($filename);
$lines = file($filename);
$last = count($lines)-1;
$tab = 0;
$space = 0;
$newLine = 0;
$character = 0;
foreach ($lines as $line_num => $line) {
    if($line_num == 0 || $line_num == $last){
        continue;
    }
    $space = $space + substr_count($line, ' ');
    $tab = (preg_match("/\t/", $line)) ? ($tab + 1) : $tab;
    $newLine = (preg_match("/\n/", $line)) ? ($newLine + 1) : $newLine;
    if (preg_match("/[^A-Za-z]/", $line)) {
        $char = preg_replace("/[^0-9A-Za-z]/", "", $line);
        $character = $character + strlen($char);
    }

}

$totalChars = $space + $tab + $newLine + $character;
$files[] = array(
    'filename' => basename($filename), 
    'characters' => $totalChars
);

endforeach;


?>
